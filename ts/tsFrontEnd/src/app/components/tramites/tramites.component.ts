import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroupDirective, NgForm, Validators, FormBuilder, FormGroup} from '@angular/forms';
import {MatTableDataSource} from '@angular/material';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: string;
  symbol: string;
  dem: string;
  tram: string;
}
const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Jose', weight: 'Juez 1', symbol: 'Acciones Mineras', dem: 'ana', tram: 'Observado'},
  {position: 2, name: 'Alejandro', weight: 'Juez 2', symbol: 'Actos Municipales', dem: 'pedro', tram: 'No Observado'},
  {position: 3, name: 'Julio', weight: 'Juez 1', symbol: 'Actos Municipales', dem: 'juan', tram: 'Observado'},
  {position: 4, name: 'Boris', weight: 'Juez 1', symbol: 'Impugnacion por actos administrativos', dem: 'maria', tram: 'Observado'},
  {position: 5, name: 'Yony', weight: 'Juez 2', symbol: 'Impugnacion de Resoluciones emitidas por el SENASIG', 
  dem: 'jonathan', tram: 'Observado'},
  {position: 6, name: 'Marcelo', weight: 'Juez 2', symbol: 'Actos Municipales', dem: 'juan pablo', tram: 'Observado'},
  {position: 7, name: 'Carla', weight: 'Juez 3', symbol: 'Impugnacion por actos administrativos', dem: 'paola', tram: 'No Observado'},
  {position: 8, name: 'Casimiro', weight: 'Juez 3', symbol: 'Actos Municipales', dem: 'claudia', tram: 'No Observado'},
  {position: 9, name: 'Federico', weight: 'Juez 2', symbol: 'Impugnacion de Resoluciones emitidas por el SENASIG',
   dem: 'fernanda', tram: 'Observado'},
  {position: 10, name: 'Constancio', weight: 'Juez 1', symbol: 'Impugnacion por actos administrativos', dem: 'diego', tram: 'No Observado'},
];


@Component({
  selector: 'app-tramites',
  templateUrl: './tramites.component.html',
  styleUrls: ['./tramites.component.css']
})
export class TramitesComponent implements OnInit {

  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol', 'dem', 'tram'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;

  demandanteFormControl = new FormControl('', [Validators.required]);
  demandadoFormControl = new FormControl('', [Validators.required]);
  telefonoFormControl = new FormControl('', [Validators.required]);

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  constructor(private _formBuilder: FormBuilder) { }

  ngOnInit() {
  }

}
